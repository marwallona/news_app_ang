export interface INews {
    nid: number,
    news_title: string,
    cat_id: number,
    news_date: Date,
    news_image: string,
    news_description: string,
    video_url: string,
    video_id: string,
    content_type: string,
    category_name: string,
    comments_count: string,
    favorite: number,
    news_gallery: string,
}