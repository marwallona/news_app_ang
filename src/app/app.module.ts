import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { EqtesadComponent } from './eqtesad/eqtesad.component';
import { FooterComponent } from './footer/footer.component';
import {FormsModule} from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
//backend libary call for http
import { HttpClientModule } from '@angular/common/http';
import { NewsDetailsComponent } from './news-details/news-details.component';
import { CategoryComponent } from './category/category.component';

import { SearchComponent } from './search/search.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';

import { AngularFireAuthModule  } from "@angular/fire/auth";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavbarComponent,
    HomeComponent,
    EqtesadComponent,
    FooterComponent,
    NewsDetailsComponent,
    CategoryComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    ToastrModule.forRoot(), // ToastrModule added
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 

  
}
