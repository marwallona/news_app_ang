import { Component, OnInit,Input } from '@angular/core';
//call service to access functions
import {ServicesService} from  '../services.service';
import { AngularWaitBarrier } from 'blocking-proxy/built/lib/angular_wait_barrier';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  public arrRecentNewsThree=[];
  public arrRecentNews;

  public arrUrgentNews;
  @Input() arrCategories;
  // public arrCategoriesMain;
  public CategoryOneNew =[];
  public catSport;
  public CategoryOneNewBeta;

  public isLoadRNews=false;
  public isLoadUNews=false;
  public isLoadRNewsThree=false;
  public isLoadRNewsTwo=false;
 
  public isLoadCatTwo=false;
  public isLoadCatOne=false;
  // public isLoadCat=false;

  @Input() isLoadCat;
  

  constructor(private _services:ServicesService, private router: Router) { }
  public MediaUrl = this._services.baseMediaUrl;
  ngOnInit() {
    this.getRnews();
    this.getUnews();
    
    // this.getAllCatwithLimitTwo();
    // this.getAllCatwithLimitOne();
  }

  
  getRnews(){
    this._services.getRecentNews().subscribe(
      (data:Response)=>{
          this.arrRecentNews = data['posts'];
          this.isLoadRNews = true;
      }
      );
  }
  getUnews(){
    this._services.getUrgentNews().subscribe(
      (data:Response)=>{
          this.arrUrgentNews = data['posts'];
          
          this.isLoadUNews = true;
      }
      );
  }

  
  getNewsDetails(id){
    this.router.navigate(['/news', id]);
  }
  
  getNewsAttachedCategory(id){
    this.router.navigate(['/category', id]);
  }

}
