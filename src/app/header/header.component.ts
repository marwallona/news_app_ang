import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import {ServicesService} from  '../services.service';

import { ToastrService } from 'ngx-toastr';
import { auth } from 'firebase/app';
import { AngularFireAuth  } from "@angular/fire/auth";
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public objRegister = {'username':'','email':'','password':''};
  public user;
  public objLogin = {'email':'','password':''};
  // changeDetectorRef: ChangeDetectorRef
  constructor(private _services:ServicesService, private toastr: ToastrService,public afAuth: AngularFireAuth,public changeDetectorRef: ChangeDetectorRef) { 
    this.changeDetectorRef = changeDetectorRef;
  }

  ngOnInit() {
  }
  register(objRegister){
    this._services.Register(objRegister).subscribe((data :Response)=>{
      if(data['result'][0]['success'] == 1){
        this._services.Login(objRegister).subscribe((data :Response)=>{
          if(data['result'][0]['success'] == 1){
            localStorage.setItem('user',JSON.stringify(data['result'][0]));
            this.toastr.success('نجاح','تم النسجيل بنجاح');
            this.objRegister = {'username':'','email':'','password':''};
          }else{
            if(data['result'][0]['msg'] ="Email address already used"){
              this.toastr.error('تم التسجيل قبل ذللك بهذا بريد الالكتروني ','خطا');
            }else{
              this.toastr.error('خطا','يوجد خطا اعد المحاوله');
            }
            
          }
        })
      }else{
        
      }
    })
  }
  
  login(objLogin){
    this._services.Login(objLogin).subscribe((data :Response)=>{
      if(data['result'][0]['success'] == 1){
        localStorage.setItem('user',JSON.stringify(data['result'][0]));
        this.toastr.success('نجاح','تم دخول بنجاح');
        this.objLogin = {'email':'','password':''};;
        

        // this.childEvent.emit( localStorage.setItem('user',JSON.stringify(data['result'][0])));
  
        
      }else{
        
        this.toastr.error('خطا','اعد محوله مره اخرى');
        
      }
    })
  }


  FacebookAuth() {
    
   return this.AuthLogin(new auth.FacebookAuthProvider());
 }  
 GoogleAuth() {
   return this.AuthLogin(new auth.GoogleAuthProvider());
 }  

AuthLogin(provider) {
  // this.obj.user = 'mirette';
  console.log('hello');
  return this.afAuth.auth.signInWithPopup(provider)
  .then(result => {
    console.log(result['additionalUserInfo']['profile']);
   this.objRegister.email = result['additionalUserInfo']['profile']['email'];
   this.objRegister.username = result['additionalUserInfo']['profile']['name'];
   this.objRegister.password = result['additionalUserInfo']['profile']['id'];
  //  this.register(this.objRegister);
  
    // this.obj.bol = 'false';
    this.changeDetectorRef.detectChanges();

    // console.log(this.objRegister.email);
      // console.log(result['additionalUserInfo']['profile']);
      // console.log('You have been successfully logged in!')
  }).catch((error) => {
      console.log(error)
  })
  
  
}

  
  
}
