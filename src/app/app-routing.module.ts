import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent} from './home/home.component';


import { EqtesadComponent} from './eqtesad/eqtesad.component';
import {NewsDetailsComponent} from './news-details/news-details.component';
import {CategoryComponent} from './category/category.component';


const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'news/:id',component:NewsDetailsComponent,runGuardsAndResolvers: 'always'},
  {path:'category/:id',component:CategoryComponent},
  {path:'eqtesad',component:EqtesadComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,  { useHash:true,onSameUrlNavigation:"reload" })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
