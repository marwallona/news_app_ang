import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EqtesadComponent } from './eqtesad.component';

describe('EqtesadComponent', () => {
  let component: EqtesadComponent;
  let fixture: ComponentFixture<EqtesadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EqtesadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EqtesadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
