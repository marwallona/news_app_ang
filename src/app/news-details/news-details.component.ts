import { Component, OnInit,Input } from '@angular/core';
import {ServicesService} from '../services.service';
import {ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';

@Component({
  selector: 'app-news-details',
  templateUrl: './news-details.component.html',
  styleUrls: ['./news-details.component.css']
})
export class NewsDetailsComponent implements OnInit {
  public MediaUrl = this._services.baseMediaUrl;
  public news_id;
  public news;
  public arrRecentNews;
  public arrUrgentNews;
  public isLoadRNews=false;
  public isLoadNews= false; 
  public arrPreferredNews = [];
  @Input() isLoadCat;
  constructor(private _services:ServicesService,private route :ActivatedRoute,private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(routeParams => {
      this.getData();
      this.onActivate(event);
  });
    
    
  }
  getData(){
    this.isLoadRNews=false;
    this.isLoadNews= false;
    let id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.news_id = id;
    
    this._services.getNewsDetails(this.news_id).subscribe(
      (data:Response)=>{
        this.news = data['post'];
        // this.news['news_image'] = this._services.baseMediaUrl + this.news['news_image'] ;
        this.getPreferedNews(this.news['cat_id'])
        this.isLoadNews = true;
      }
    );
    this.getRnews();
    this.getUnews();
  }
  getPreferedNews(category_id){
    this._services.getNewsCategory(category_id).subscribe(
      (data:Response)=>{
        this.arrPreferredNews = data['posts'];
       
        // this.category = data['category']
        // this.isLoadCategory = true;
      }
    );
  }
  getRnews(){
    this._services.getRecentNews().subscribe(
      (data:Response)=>{
          this.arrRecentNews = data['posts'];
          // this.arrRecentNews.forEach(obj => {
          //   obj['news_image'] = this._services.baseMediaUrl + obj['news_image'];
          // });
          this.isLoadRNews = true;
      }
      );
  }
  
  getNewsDetails(id){

    this.router.navigate(['/news', id]);
  }
  getUnews(){
    this._services.getUrgentNews().subscribe(
      (data:Response)=>{
          this.arrUrgentNews = data['posts'];
          // this.isLoadUNews = true;
    });
  }
  onActivate(event) {
    window.scroll(0,0);
    
  }
}
