import { Component, OnInit, Input } from '@angular/core';
import {ServicesService} from '../services.service';
import {ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';
import * as $ from 'jquery';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  public MediaUrl = this._services.baseMediaUrl;
  public category_id;
  public category;
  public arrNews =[];
  public arrRecentNews = [];
  public isLoadRNews = false;
  public isLoadUNews = false;
  public isLoadCategory= false;
  public arrUrgentNews = [];
  @Input() isLoadCat;
  constructor(private route :ActivatedRoute,private _services:ServicesService, private router: Router) { }

  ngOnInit() {
    
    this.route.params.subscribe(routeParams => {
      this.getData();
      this.onActivate(event);
  });
    
    
  }
  getData(){
    this.isLoadCategory = false;
    this.isLoadRNews = false;
    let id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.category_id = id;
    
    this._services.getNewsCategory(this.category_id).subscribe(
      (data:Response)=>{
        this.arrNews = data['posts'];
       
        this.category = data['category']
        this.isLoadCategory = true;
      }
    );
    this.getRnews();
    // this.getUnews();
  }
  getUnews(){
    this._services.getUrgentNews().subscribe(
      (data:Response)=>{
          this.arrUrgentNews = data['posts'];
          
          this.isLoadUNews = true;
      }
      );
  }
  getRnews(){
    this._services.getRecentNews().subscribe(
      (data:Response)=>{
          this.arrRecentNews = data['posts'];
          // this.arrRecentNews.forEach(obj => {
          //   obj['news_image'] = this._services.baseMediaUrl + obj['news_image'];
          // });
          this.isLoadRNews = true;
      }
      );
  }

  getNewsDetails(id){
    this.router.navigate(['/news', id]);
  }
  onActivate(event) {
    window.scroll(0,0);
    
  }

  

}
