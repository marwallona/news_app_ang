import { Injectable } from '@angular/core';
//call backend after add it in app.module.ts
import {HttpClient ,HttpHeaders} from '@angular/common/http';

import 'rxjs/add/operator/map'

@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  public baseUrl = "http://qhadath.com/qhadath/newsAppByHouida/api/";
  // public baseUrl = "http://localhost:8080/newsapp_byhouida/api/";
  public baseMediaUrl = "http://qhadath.com/qhadath/newsAppByHouida/upload/";
  // public baseMediaUrl = "http://localhost:8080/newsapp_byhouida/upload/";
  private _urlR: string = this.baseUrl+"get_recent_posts?api_key=cda11v2OkqSI1rhQm37PBXKnpisMtlaDzoc4w0U6uNATgZRbJG&page=4";
  private _urlU: string = this.baseUrl+"get_urgent_posts?api_key=cda11v2OkqSI1rhQm37PBXKnpisMtlaDzoc4w0U6uNATgZRbJG";
  private _urlD: string = this.baseUrl+"get_post_detail?api_key=cda11v2OkqSI1rhQm37PBXKnpisMtlaDzoc4w0U6uNATgZRbJG&id="
  private _urlNC: string = this.baseUrl+"get_category_posts?api_key=cda11v2OkqSI1rhQm37PBXKnpisMtlaDzoc4w0U6uNATgZRbJG&id="
  private _urlC: string = this.baseUrl+"get_all_categories?api_key=cda11v2OkqSI1rhQm37PBXKnpisMtlaDzoc4w0U6uNATgZRbJG"
  private _urlRegister: string = this.baseUrl+"user_register_website?api_key=cda11v2OkqSI1rhQm37PBXKnpisMtlaDzoc4w0U6uNATgZRbJG";
  private _urlL: string = this.baseUrl+"user_login_website?api_key=cda11v2OkqSI1rhQm37PBXKnpisMtlaDzoc4w0U6uNATgZRbJG";
  // to use http you need to call it in constructor 
  constructor(private http: HttpClient) { }

  Register(userData){
   
    return this.http.post(this._urlRegister,JSON.stringify(userData),
        {headers: new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded',
          Accept: '*/*',
          
        })}
      ).map(data=>
        data);

  };
  Login(userData){
    return this.http.post(this._urlL,JSON.stringify(userData),
      {headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: '*/*',
        
      })}
    ).map(data=>
      data);

  }


  getRecentNews(){
    return this.http.get(this._urlR);
  }
  getUrgentNews(){
    return this.http.get(this._urlU);
  }

  getNewsDetails(id){
    return this.http.get(this._urlD+id);
  }
  
  getNewsCategory(id){
    return this.http.get(this._urlNC+id);
  }


  getAllCategories(){
    return this.http.get(this._urlC);
  }

  
}
