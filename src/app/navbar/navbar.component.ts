import { Component, OnInit ,Input, Output, EventEmitter } from '@angular/core';
import {ServicesService} from  '../services.service';
import {ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public arrCategories = [];
  // @Output() isLoadCat:EventEmitter<any> = new EventEmitter();
  @Output() isLoadCatEmitter:EventEmitter<any> = new EventEmitter();
  @Output() arrCategoriesEmitter:EventEmitter<any> = new EventEmitter();
  // public isLoadCat = 'false';
  constructor(private _services:ServicesService,private router: Router) { }

  ngOnInit() {
    
    this.getAllCat();
  }
  getAllCat(){
    this._services.getAllCategories().subscribe((data:Response)=>{
      this.arrCategories = data['categories']
      this.arrCategoriesEmitter.emit(data['categories']);
      this.isLoadCatEmitter.emit(true);
      // sessionStorage.setItem('lang',this.isLoadCat);
     
      
    })
  }
  getNewsAttachedCategory(id){
    this.router.navigate(['/category', id]);
  }
}
