// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyA8aqOWyH0-dkApiStgLLFC4m2B04Gp2Vw",
    authDomain: "news-app-social.firebaseapp.com",
    databaseURL: "https://news-app-social.firebaseio.com",
    projectId: "news-app-social",
    storageBucket: "news-app-social.appspot.com",
    messagingSenderId: "1081025591150",
    appId: "1:1081025591150:web:d41c0675cd92eefb07b37f",
    measurementId: "G-RBSYEERSQN"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
